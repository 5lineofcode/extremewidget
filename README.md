# ExtremeWidget

<b>ExtremeWidget</b> adalah sebuah Framework Flutter yang dikembangkan oleh <b>AnagataDev</b> untuk membuat sebuah aplikasi dengan waktu yang cepat, effisien, serta kode yang mudah untuk dibaca.

Dengan menggunakan ExtremeWidget, UI dan Logic dari aplikasi benar-benar dibuat secara terpisah.
Dan juga tidak perlu membuat banyak referensi baru untuk mengakses objek di class lain karena referensinya disimpan dan bisa diakses secara <b>global</b>.

Untuk saat ini framework ini masih sedang tahap pengembangan dan masih digunakan untuk keperluan Internal.
Namun kami tidak menutup kemungkinan bahwa framework ini akan digunakan oleh banyak orang, baik <b>gratis</b> maupun <b>freemium</b>.

Untuk architecture, kami menggunakan <b>MVC</b>
Sedangkan backend, kami fokus untuk mengintegrasikannya dengan <b>Firebase</b>.
